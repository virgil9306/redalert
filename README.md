# README #

RA is an Android app notification system to inform others of when your phone's battery is about to die. On your way to meet someone? Forgot to charge your phone? Don't realize that your phone is about to die? No problem! They already know, so they're not going to worry. On your way to the office, but running late? Hung over and forgot to charge your phone last night *and* forgot to message your team? No problem, they already know that your phone's battery died! Played too much P*kemon Go and your phone died in your pocket before you got to the club? Don't worry, RA has you covered and your peeps know you didn't just bail on them.

A small tool that you can set and forget, to make your life *just* a bit more convenient for those small emergencies that we all forget about.

### Repository Contents ###

* Android App source

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Use Javadoc-style comments
* Writing tests in JUnit strongly encouraged
* Feature requests to be made as a "proposal" issue type

### Who do I talk to? ###

* Repo owner or admin
* Other community members