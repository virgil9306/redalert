package com.example.vg.ra;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * Created by vincent.graziano on 1/4/17.
 */
public class RAUtil {
    private Activity activity;
    SharedPreferences sharedPref;

    public RAUtil(Activity _activity) {
        activity = _activity;
//        sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        sharedPref = activity.getSharedPreferences("RAUTIL", Context.MODE_PRIVATE);
    }

    public void setPreference(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();

        Log.d("[RAUtil]", "Updated K,V as <String>,<String>");
    }

    public void setPreference(String key, int value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();

        Log.d("[RAUtil]", "Updated K,V as <String>,<Int>");
    }

    public void setPreference(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();

        Log.d("[RAUtil]", "Updated K,V as <String>,<Boolean>");
    }

    public boolean getPreferenceBoolean (String str) {
        boolean ret = sharedPref.getBoolean(str, false);
        return ret;
    }

    public int getIntFromKey (String str) {


        Context context = activity.getApplicationContext();
        int ret = sharedPref.getInt(str, context.getResources().getInteger(R.integer.ERROR_INTEGER));
        return ret;
    }

    public String getStringFromKey (String str) {
        Context context = activity.getApplicationContext();
        String ret = sharedPref.getString(str, context.getResources().getString(R.string.stg_message_default));
        return ret;
    }

    public int getIntFromName (int i) {
        Context context = activity.getApplicationContext();
        int ret = context.getResources().getInteger(i);
        return ret;
    }

    public void clearAll() {
        sharedPref.edit().clear().commit();
    }

    public void clearTwitterPreferences (String key, String secret, Context _context) {

        // Try the method at:
        // http://stackoverflow.com/questions/11046669/twitter4j-how-do-i-logout
        CookieSyncManager.createInstance(_context); // Pass in the context
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();

        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(key, secret);
        twitter.setOAuthAccessToken(null);
//        twitter.shutdown(); // There is no Shutdown anymore?
        
        // If that fails, then try the below:
//        SharedPreferences.Editor e = sharedPref.edit();
//        e.remove(key);
//        e.remove(secret);
//        e.remove(PREF_KEY_TWITTER_LOGIN);
//        e.commit();
//        //twitter.shutdown();
//        twitter = new TwitterFactory().getInstance();
//        twitter.setOAuthConsumer(key, secret);
    }


}
