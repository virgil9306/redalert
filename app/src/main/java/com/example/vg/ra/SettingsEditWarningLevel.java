package com.example.vg.ra;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsEditWarningLevel extends AppCompatActivity {
    Button btn_confirm;
    RAUtil u;
    SeekBar sk;
    TextView percentageText;
    int setting;
    String setting_suffix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_edit_warning_level);
        u = new RAUtil(this);

        // Drag Handle
        percentageText = (TextView) findViewById(R.id.levelPercentage);
        assert percentageText != null;

        // Set default
        setting = u.getIntFromKey(getString(R.string.BATTERY_PERCENTAGE_STG));
        int ERROR = getApplicationContext().getResources().getInteger(R.integer.ERROR_INTEGER);
        int default_val = getApplicationContext().getResources().getInteger(R.integer.BATTERY_PERCENTAGE_DEFAULT);
        int min = getApplicationContext().getResources().getInteger(R.integer.BATTERY_PERCENTAGE_MIN);
        int max = getApplicationContext().getResources().getInteger(R.integer.BATTERY_PERCENTAGE_MAX);

        sk = (SeekBar) findViewById(R.id.seekBar_WarningLevel);
        assert sk != null;
        sk.setMax(max);

        // If default / error
        if (setting == ERROR) {
            setting = default_val;
            u.setPreference(Integer.toString(R.string.BATTERY_PERCENTAGE_STG), setting);
        }

        // Proceed to set text
        setting_suffix = getApplicationContext().getString(R.string.stg_button_percent_suffix);
        sk.setProgress(setting);
        percentageText.setText(Integer.toString(setting) + setting_suffix);

        // Bug workaround (must set max to 0 then max to max, then set progress again)
        sk.setProgress(min);
        sk.setProgress(max);
        sk.setProgress(setting);

        // Change listener
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                percentageText.setText(Integer.toString(progress) + setting_suffix);
                u.setPreference(getString(R.string.BATTERY_PERCENTAGE_STG), progress);
            }
        });

        // Button: Settings
        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        assert btn_confirm != null;
        u = new RAUtil(this);

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int progress = sk.getProgress();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",progress);
                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        finish();
    }
}
