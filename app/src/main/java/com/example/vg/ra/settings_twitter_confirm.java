package com.example.vg.ra;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link settings_twitter_confirm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link settings_twitter_confirm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class settings_twitter_confirm extends Fragment implements View.OnClickListener {
    private OnFragmentInteractionListener mListener;
    private View view;

    public settings_twitter_confirm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment settings_twitter_confirm.
     */
    // TODO: Rename and change types and number of parameters
    public static settings_twitter_confirm newInstance(String param1, String param2) {
        settings_twitter_confirm fragment = new settings_twitter_confirm();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
        }
        Log.d(">>>>>>>>>>FRAGMENT:","onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(">>>>>>>>>>FRAGMENT:","onCreateView");

        view = inflater.inflate(R.layout.fragment_settings_twitter_confirm, container, false);

        // SET UP BUTTON LISTENER????
        Button btn = (Button) view.findViewById(R.id.setting_btn_twitter_pin_confirm);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int SDK_INT = android.os.Build.VERSION.SDK_INT;
                if (SDK_INT > 8) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    //your codes here

                    EditText mEdit;
                    mEdit = (EditText) view.findViewById(R.id.text_twitter_pin_field);

                    String pinvalue = mEdit.getText().toString();
                    onButtonPressed(pinvalue);
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String input) {
        Log.d(">>>>>>>>>>FRAGMENT:","onClick of BTN");
        if (mListener != null) {
            mListener.onFragmentInteraction(input);
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(">>>>>>>>>>FRAGMENT:","onAttach");

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        ((SettingsActivity) getActivity()).TwitterVerify("TEST");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String input);
    }
}
