package com.example.vg.ra;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class MainActivity extends AppCompatActivity {
    private Button btn_stg;
    RAUtil u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);   // unused
//        setSupportActionBar(toolbar);                             // unused

        // Util: Instantiate
        u = new RAUtil(this);

        // State: On/Off
        boolean active = u.getPreferenceBoolean(getString(R.string.STATUS_ACTIVE));

        // Button: On/Off
        final Button btn_onoff = (Button) findViewById(R.id.btn_onoff);
        assert btn_onoff != null;

        toggleCheck(active, btn_onoff);

        btn_onoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Status
                boolean _active = u.getPreferenceBoolean(getString(R.string.STATUS_ACTIVE));
                boolean new_state = !_active;

                // Display Status with toast
                Context context = getApplicationContext();
                Toast toast;
                String msg;

                if (new_state == false) {
                    msg = getString(R.string.status_turned_off);
                } else {
                    msg = getString(R.string.status_turned_on);
                }
                // Show snackbar
                Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                // Set Status
                u.setPreference(getString(R.string.STATUS_ACTIVE), new_state);
                // Toggle Check
                toggleCheck(new_state, btn_onoff);
            }
        });

        // Button: Settings
        btn_stg = (Button) findViewById(R.id.btn_stg);
        assert btn_stg != null;
        btn_stg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        // POWER STATE
        // PowerConnectionReceiver pcr = new PowerConnectionReceiver(getApplicationContext());
        Intent updateIntent = new Intent(this, UpdateService.class);
        startService(updateIntent);

        // DEFAULT SETTING: PERCENT OF ALERT
        // (If no alert percentage, set to default value)

        int alert_value = u.getIntFromKey(getString(R.string.BATTERY_PERCENTAGE_STG));

//        u.clearAll();

        Log.d("[MainActivity]","Alert value: " + alert_value + "; Default value: " + u.getIntFromName(R.integer.BATTERY_PERCENTAGE_DEFAULT));

        if (alert_value == R.integer.ERROR_INTEGER) { // parameter unset
            u.setPreference(getString(R.string.BATTERY_PERCENTAGE_STG), u.getIntFromName(R.integer.BATTERY_PERCENTAGE_DEFAULT));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleCheck(boolean active, Button btn) {
        toggleCheckButton(active, btn);
        toggleCheckBackgroundColor(active);
        toggleCheckStatusIcon(active);
    }

    private void toggleCheckButton(boolean active, Button btn) {
        if (active == false) { // is currently off
            btn.setText(getString(R.string.status_turn_on));
        } else { // is currently on
            btn.setText(getString(R.string.status_turn_off));
        }
    }

    private void toggleCheckBackgroundColor(boolean active) {
        int colorOn  = ContextCompat.getColor(this, R.color.colorPrimary);
        int colorOff = ContextCompat.getColor(this, R.color.colorSecondary);
        int colorOnSecondary  = ContextCompat.getColor(this, R.color.colorPrimaryDark);
        int colorOffSecondary = ContextCompat.getColor(this, R.color.colorSecondaryDark);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout_main);

        if (active == false) {
            layout.setBackgroundColor(colorOff);
            // Set title bar color too
            View view = this.getWindow().getDecorView();
            view.setBackgroundColor(colorOffSecondary);
        } else {
            layout.setBackgroundColor(colorOn);

            // Set title bar color too
            View view = this.getWindow().getDecorView();
            view.setBackgroundColor(colorOnSecondary);
        }
    }

    private void toggleCheckStatusIcon (boolean active) {
        ImageView iv = (ImageView) findViewById(R.id.img_status_icon);
        if (active == false) {
            // Set button text
            iv.setImageResource(R.drawable.off);
        } else {
            iv.setImageResource(R.drawable.on);
        }
    }
}
