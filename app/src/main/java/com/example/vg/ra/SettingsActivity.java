package com.example.vg.ra;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class SettingsActivity extends AppCompatActivity implements settings_twitter_confirm.OnFragmentInteractionListener{
    private RequestToken requestToken;
    Button btn_config_twitter;
    Button btn_config_message;
    Button btn_config_warnlevel;
    RAUtil u;
    int error;
    int REQUEST_CODE_WARNING_LEVEL_SETTING = 1;
    int RESULT_DEFAULT_OR_FAILED = -1;
    boolean logged_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        u = new RAUtil(this);
        error = getApplicationContext().getResources().getInteger(R.integer.ERROR_INTEGER);

        // Hide the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        /*
         * Button: Message
         */
        btn_config_message = (Button) findViewById(R.id.setting_btn_editmessage);
        btn_config_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, SettingsEditMessage.class);
                startActivityForResult(intent, REQUEST_CODE_WARNING_LEVEL_SETTING);
            }
        });

        /*
         * Button: Warning
         */

        btn_config_warnlevel = (Button) findViewById(R.id.setting_btn_warn);
        assert btn_config_warnlevel != null;
        SetPercentageButtonText();
        btn_config_warnlevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, SettingsEditWarningLevel.class);
                startActivityForResult(intent, 1);
            }
        });


        /*
         * Button: Twitter Login/Logout
         */
        btn_config_twitter = (Button) findViewById(R.id.btn_config_twitter);

        TwitterButtonTextToggle();
        btn_config_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TwitterToggle();
                TwitterButtonTextToggle();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // assuming used for moving to parent
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_WARNING_LEVEL_SETTING) {
            if(resultCode == AppCompatActivity.RESULT_OK){
                int result = data.getIntExtra("result", RESULT_DEFAULT_OR_FAILED);
                if (result != RESULT_DEFAULT_OR_FAILED) {
                    SetPercentageButtonText();
                }
            }
            if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void SetPercentageButtonText () {
        String level_prefix = getString(R.string.stg_button_percent_prefix);
        String level_suffix = getString(R.string.stg_button_percent_suffix);
        String level_string;

        int level;
        int level_default;

        level = u.getIntFromKey(getString(R.string.BATTERY_PERCENTAGE_STG));
        level_default = getApplicationContext().getResources().getInteger(R.integer.BATTERY_PERCENTAGE_DEFAULT);
        if (level == error) {
            level = level_default;
        }

        level_string = Integer.toString(level);
        btn_config_warnlevel .setText(level_prefix + level_string + level_suffix);
    }

    private void dealWithTwitterResponse(Intent intent) {
        ShowMessage("Twitter Response Received");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        dealWithTwitterResponse(intent); // Unnecessary When going BACK to settings screen this fires.
    }

    private void ShowMessage (String msg) {
        View view = (RelativeLayout) findViewById(R.id.content_settings);
        assert view != null;
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void TwitterToggle() {
        logged_in = u.getPreferenceBoolean(getString(R.string.TWITTER_LOGIN_STATUS));
        if (logged_in == true) {
            Log.d(">>>TwitterToggle","You are LOGGED IN, thus going to LOG OUT");
            // 1. Begin logout procedure
            // 1.a. Show snackbar
            ShowMessage(getString(R.string.twitter_logout_success));

            // 1.b. Delete local stuff to disable Twitter from being usable
            // since there is no true "log-out" when using Twitter4j
            String key = getString(R.string.twitter_consumer_key);
            String secret = getString(R.string.twitter_consumer_secret);
            Context _context = getApplicationContext();
            u.clearTwitterPreferences(key, secret, _context);

            // 2. If we succeed to log out
            u.setPreference(getString(R.string.TWITTER_LOGIN_STATUS), false);
        } else {
            Log.d(">>>TwitterToggle","You are NOT logged in, thus we will try to LOG YOU IN");
            // 1. Begin login procedure
            // 1.a. Show snackbar
            ShowMessage(getString(R.string.twitter_login_prompt));
            // 1.b. Prompt sign-on
            try {
                Log.d(">>>>>","Logging");
                TwitterResponse();
                // 2. if we succeed to log in
                u.setPreference(getString(R.string.TWITTER_LOGIN_STATUS), true);
            } catch (Exception e) {
                u.setPreference(getString(R.string.TWITTER_LOGIN_STATUS), false);
                e.printStackTrace();
            }
        }
    }

    private void TwitterButtonTextToggle () {
        logged_in = u.getPreferenceBoolean(getString(R.string.TWITTER_LOGIN_STATUS));
        if (logged_in == true) {
            btn_config_twitter.setText(getText(R.string.btn_logout));
        } else {
            btn_config_twitter.setText(getText(R.string.btn_login));
        }
    }

    private void TwitterResponse () throws Exception {
        // Do network action in this function
        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    // 1. Get URL from Twitter
                    String url;
                    String key = getString(R.string.twitter_consumer_key);
                    String secret = getString(R.string.twitter_consumer_secret);
                    requestToken = TestTwitterPosting.GetToken(key, secret);
                    url = requestToken.getAuthorizationURL();

                    // 2. Open the browser on top of that intent
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(browserIntent);
                    // @todo Change to this state: http://stackoverflow.com/questions/12367602/android-browser-intent-back-button

                    // 3. Open intent for user to input so when they return they can input the code
                    //    THIS is the intent (fragment) that should handle the sections:
                    //    // After that activity ends, open the PIN verification activity
                    //    //persist to the accessToken for future reference.
                    //    // Twitter connection & settings are contained therein.

                    // Begin the transaction
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.addToBackStack("TwitterResponse Method");
//                    ft.commit(); // Complete the changes to the ft

                    // Replace the contents of the container with the new fragment
//                    ft.replace(R.id.fragment_placeholder, new settings_twitter_confirm());
                    ft.add(R.id.fragment_placeholder, new settings_twitter_confirm());

                    ft.commit(); // Complete the transaction

                    // 4. Try to get PIN from user when they come back & push "OK"
                    //    This logic is done via interface w/the fragment, see onFragmentInteraction
                } catch (Exception e) {
                    // TODO
                }
            }
        }).start();
    }

    public boolean TwitterVerify (String pin) { // maybe this'd better go below
        AccessToken accessToken = null;
        Twitter twitter = TwitterFactory.getSingleton();

        boolean ret;

        try{
            if(pin.length() > 0){
                accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                ret = true;
            } else {
                accessToken = twitter.getOAuthAccessToken();
                ret = false;
            }
        } catch (TwitterException te) {
            ret = false;
            if (401 == te.getStatusCode()){
                System.out.println("Unable to get the access token.");
            } else {
                te.printStackTrace();
            }
        }

        return ret;
    }

    @Override
    public void onFragmentInteraction(String input) {
        if (input.length() > 0) {
            Log.d("[RA:SettingsActivity]","[SettingsActivity.java]onFragmentInteraction. User input " + input);
            if (TwitterVerify(input)) {
                Log.d("[RA:SettingsActivity]", "Code input success!!!");
                try {
                    TestPost();
                    PopBackFragment();
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("[RA:SettingsActivity]", "Wrong code/no input/valid pin input but used more than once");
            }
        } else {
            Log.d("[RA:SettingsActivity]","[SettingsActivity.java]onFragmentInteraction. User input NOTHING!");
            ShowMessage("Please enter the PIN # from Twitter");
        }
    }

    private void PopBackFragment () {
        FragmentManager fm = getSupportFragmentManager();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void TestPost () throws TwitterException {
        String date = getDate();

        Twitter twitter = TwitterFactory.getSingleton();
        Status status = twitter.updateStatus("This is a test @ " + date); // test update
    }

    private String getDate() {
        Calendar c = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        String strDate = sdf.format(c.getTime());

        return strDate;
    }
}
