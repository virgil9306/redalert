package com.example.vg.ra;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by vincent.graziano on 9/20/16.
 */
public class PowerConnectionReceiver extends BroadcastReceiver {
    private static Context context;
    private static NotificationManager notificationManager;
    public static float batteryAmt = 0;
    private static Notification.Builder builder;
    public static int notifid = 001;

    String notification_prefix;
    String context_text;
    String notification_suffix;
    String subtext_prefix;
    String subtext_suffix;
    String subtext_mid;

    public PowerConnectionReceiver(Context _context) {
        context = _context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE); // set these context stuff in the constructor
        createNotification("Hello world yo");

        context.registerReceiver(this, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    void createNotification(String title) {
        notification_prefix = context.getString(R.string.notification_prefix);
        context_text = context.getString(R.string.notification_context);
        notification_suffix = context.getString(R.string.notification_suffix);
        subtext_prefix = context.getString(R.string.notification_subtext_prefix);
        subtext_suffix = context.getString(R.string.notification_subtext_suffix);
        subtext_mid    = "";

        builder = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setTicker("Test ticker")
                .setContentTitle("Title test")
                .setContentText(context_text)
                .setSubText(subtext_prefix + " " + subtext_mid + subtext_suffix)
                .setOngoing(true)
                .setAutoCancel(false);

        notificationManager.notify(notifid, builder.build());
        // Notification manager on open should prompt the user to a Share dialog to share their battery status
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("BATTERY CHANGED******", ":: IN POWERCONNECTIONRECEIVER");

        // Check battery state
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = (status == BatteryManager.BATTERY_STATUS_CHARGING) ||
                (status == BatteryManager.BATTERY_STATUS_FULL);

        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);


        batteryAmt = level / (float)scale;
        int batteryPercent = (int) (batteryAmt * 100);

        if (builder != null) { // try/catch for builder not being initialized yet?
            builder.setContentTitle(notification_prefix + " " + batteryPercent + notification_suffix);
            notificationManager.notify(notifid, builder.build());
        }

        // Send to Twitter
    }
}