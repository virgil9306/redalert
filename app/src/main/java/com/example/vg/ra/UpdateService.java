package com.example.vg.ra;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by vincent.graziano on 9/21/16.
 */

public class UpdateService extends Service {
    private static final String TAG = ">>>>>>>>>>";
    private PowerConnectionReceiver pcr;
    // BELOW IS UNUSED
//    private final BroadcastReceiver br = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.i("BATTERY CHANGED******", ":: IN UPDATESERVICE");
//        }
//    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Service bound!!!");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
        pcr = new PowerConnectionReceiver(getApplicationContext());
        // BELOW IS UNUSED
//        IntentFilter batteryStatusIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
//        Intent batteryStatusIntent = registerReceiver(br, batteryStatusIntentFilter);
    }
}
