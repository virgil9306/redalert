package com.example.vg.ra;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SettingsEditMessage extends AppCompatActivity {
    EditText et;
    Button btn_editmessage;
    RAUtil u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_edit_message);

        u = new RAUtil(this);

        et = (EditText) findViewById(R.id.editTextMessage);

        String status = u.getStringFromKey(getString(R.string.ALERT_MESSAGE));

        et.setText(status);

        // Button: Settings
        btn_editmessage = (Button) findViewById(R.id.setting_btn_editmessage);
        assert btn_editmessage != null;

        btn_editmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status_new = et.getText().toString();
                u.setPreference(getString(R.string.ALERT_MESSAGE), status_new);

                Intent returnIntent = new Intent();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}
